class Post < ApplicationRecord
	belongs_to :user
	
	# every post must have content
	validates_presence_of :content
	
	# validate :title for alpha threads
	if :type === 'alpha'
		validates_presence_of :title
	end

	# validates for only :content for beta & gamma threads
	if :type != 'alpha'
		validates_absence_of :title
		# the content is limited to 256 characters if !alpha
	end
end
